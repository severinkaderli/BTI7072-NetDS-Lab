name: n113
networks-internal:
  ipv4: 193.5.82.128/27
  ipv6: 2001:620:500:FF0D::/64
router-external:
  ipv4: 193.5.80.113
  ipv6: 2001:620:500:FF00::FF0D
  locl: FE80::FF0D
router-internal
  ipv4: 193.5.82.129
  ipv6: 2001:620:500:FF0D::1
  locl: FE80::1
primary-dns:
  ipv4: 193.5.82.130
  ipv6: 2001:620:500:FF0D
dns-forward-zone: n113.nslab.ch
