---
title: "BTI7072-NetDS"
subtitle: "Journal"
author:
    - Schär Marius
    - Kaderli Severin
extra-info: true
institute: "Bern University of Applied Sciences"
department: "Engineering and Information Technology"
lecturer: "Wenger Hansjürg"
lang: "en-UK"
toc: true
lot: false
rule-color: CDDC39
link-color: 448AFF
...

# General
Severin Kaderli applied the configurations.
Marius Schär documented them in the journal and double checked them.

The students were always attending if not otherwise documented in the corresponding dates chapter.

# 2020-02-19
## Static Routing
Become root
`sudo -i`

### Configure computer name
`echo "router.n113.nslab.ch" > /etc/hostname`

`reboot` to apply this configuration.

### Configure the DNS client resolver
Set the contents of `/etc/resolv.conf` to  
```
search n113.nslab.ch nslab.ch

nameserver 193.5.80.80
```

### Configure the external Network Interface
Set the contents of `/etc/sysconfig/network-scripts/ifcfg-ens4` to:
```
BOOTPROTO=static #must be set to static for configuration
DEVICE=ens4
NAME=External_Interface
ONBOOT=yes
NM_CONTROLLED=no

IPADDR=193.5.80.113
PREFIX=24
GATEWAY=193.5.80.1

IPV6INIT=yes
IPV6ADDR=2001:620:500:FF00::FF0D/64
IPV6_DEFAULTGW=2001:620:500:FF00::1/64
```

`systemctl restart network` to apply this configuration.

### Test the internet connectivity
`ping 1.1.1.1` for testing IPv4 and  
`ping6 2606:4700:4700::1111`

# 2020-02-26
## Static Routing
### Configure the external Network Interface
Set the contents of `/etc/sysconfig/network-scripts/ifcfg-ens4` to:
```
BOOTPROTO=static #must be set to static for configuration
DEVICE=ens3
NAME=Internal_Interface
ONBOOT=yes
NM_CONTROLLED=no

IPADDR=193.5.82.129
PREFIX=27
GATEWAY=193.5.80.1

IPV6INIT=yes
IPV6ADDR=2001:620:500:FF00::FF0D/64
IPV6_DEFAULTGW=2001:620:500:FF00::1/64
```

`systemctl restart network` to apply this configuration.

Test this configuration by giving client01 the IP `193.5.82.144/27`
(the first one from the DHCP range),
with the Router Internal Interface set as the gateway.
Then `ping` the Router Internal Interface.

Using `tracepath` we determined that our IP packets weren't
being forwarded by the router.

In order to access the internet, we must enable IP forwarding on the router.
Add `net.ipv4.ip_forward = 1` to `/etc/sysctl.conf`, then `reboot`.

### Testing
Our tests can be seen in this screenshot:
![](assets/wireshark.png)

An ARP request looks like this:

`ARP  60  Who has 193.5.82.129?   Tell 193.5.82.144`

## Static Routing - Routing Tables
When pinging the router of group n114 with `ping 193.5.82.161`,
we are able to detect redirect messages:

![](assets/icmp_redirect.png)

In order to set a static route we set
`/etc/sysconfig/network-scripts/route-ens4` to

```
ADDRESS0=193.5.82.160
NETMASK0=255.255.255.224
GATEWAY0=193.5.80.114
```

`systemctl restart network` to apply this configuration.

Another capture shows us that we no longer receive an ICMP redirect,
and the Ping works as expected.

# 2020-03-04
Both students not attending the lab.

# 2020-03-11
## Dynamic Routing
### Unconfigure Static Routes
Remove the previously statically configured route:
```
rm /etc/sysconfig/network-scripts/route-ens4
```

Switch off the default configuration for the `ens3` and `ens4` interfaces:
Change `ONBOOT=yes` to `ONBOOT=no` in:

- `/etc/sysconfig/network-scripts/ifcfg-ens3`
- `/etc/sysconfig/network-scripts/ifcfg-ens4`

### Enable Zebra Logging
Append `log file /var/log/quagga/zebra.log` to `/etc/quagga/zebra.conf`.

### Enable Zebra
Start and enable zebra using systemd:
```
systemctl enable zebra.service
systemctl start zebra.service
```

### Configure Zebra
Enter the configuration interface using `vtysh`^[Virtual Tele tYpe SHell].
Configure the external interface (ens4) it using the following steps:
```
configure terminal
interface ens4
ip address 193.5.80.113/24
description External Interface
```

Configure the internal interface (ens3) it using the following steps:
```
configure terminal
interface ens3
ip address 193.5.82.129/24
description Internal Interface
```

Configure a default route:
First we tried `ip route 193.5.80.0/24 193.5.80.1`,
which doesn't work because we're redirecting all traffic destined for
the outer network onto our router.
This is the corrected config (undo the old and add the new)

```
no ip route 193.5.80.0/24 193.5.80.1
ip route 0.0.0.0/0 193.5.80.1
```

Exit the configuration mode using `exit`.
We make sure the configuration is correct using `show running-config`
Then we write it to memory using `write mem`.
Another `exit` takes us out of the vtysh.

Restart zebra using `sudo reboot`.

## Dynamic Routing - RIPv2
### Enable ripd
Create the rip config file:
```
echo "log file /var/log/quagga/ripd.log" >  /etc/quagga/ripd.conf
chown quagga:quagga /etc/quagga/ripd.conf
```

```
systemctl start ripd
```

The following commands are performed in vtysh

Enable RIP on the external interface ens4
```
conf t
router rip
network ens4
exit
```

Apply the suggested security settings
```
key chain demonet
key 1
key-string demo$rip
exit
exit
interface ens4
ip rip authentication mode md5
ip rip authentication key-chain demonet
exit
exit
write mem
```

# 2020-03-18
## Dynamic Routing - RIPv2
### Verify that we learn new routes
Within vtysh we can run `show ip route` and get the following output
```
Codes: K - kernel route, C - connected, S - static, R - RIP,
       O - OSPF, I - IS-IS, B - BGP, A - Babel,
       > - selected route, * - FIB route

R   0.0.0.0/0 [120/2] via 193.5.80.1, ens4, 02:19:58
S>* 0.0.0.0/0 [1/0] via 193.5.80.1, ens4
C>* 127.0.0.0/8 is directly connected, lo
C>* 193.5.80.0/24 is directly connected, ens4
R>* 193.5.81.224/27 [120/2] via 193.5.80.108, ens4, 02:20:14
C>* 193.5.82.128/27 is directly connected, ens3
R>* 193.5.82.160/27 [120/2] via 193.5.80.114, ens4, 00:10:19
R>* 193.5.82.224/27 [120/2] via 193.5.80.116, ens4, 02:19:59
R>* 193.5.83.96/27 [120/2] via 193.5.80.120, ens4, 00:02:09
R>* 193.5.83.128/27 [120/2] via 193.5.80.121, ens4, 02:20:01
R>* 193.5.86.224/27 [120/2] via 193.5.80.148, ens4, 02:20:03
```

Knowing that the router works, we can enable it: `sudo systemctl enable ripd`.

### RIP Packet Capture
We captured the packets using `tshark -i ens4 -w capture.pcap`.
Then display it using `tshark -r capture.pcap -Y rip -V`)
The detailed packet capture can be found in `assets/rip-capture_2020-03-18_2100.txt`.

## Dynamic routing OSPFv2
```
sudo -i
echo "log file /var/log/quagga/ospfd.log" > /etc/quagga/ospfd.conf # create the log config
chown quagga:quagga /etc/quagga/ospfd.conf
systemctl start ospdf
cat /var/log/quagga/ospdfd.log # verify successful startup
vtysh
config terminal
router ospf # configure the router to use ospf on ens4
ospf router-id 193.5.80.113 # set the router id
area 0.0.0.0 range 193.5.80.0/24    # tell OSPF which network is the Area 0 (backbone area)
area 0.0.0.0 authentication message-digest
network 193.5.80.0/24 area 0.0.0.0
exit
interface ens4
ip ospf authentication message-digest
ip ospf message-digest-key 1 md5 demo$ospf
exit
exit
write mem
```

# 2020-03-19
## Dynamic routing OSPFv2
### Current Config
Showing the current OSPF configuration using `show ip ospf` within `vtysh`:
```
 OSPF Routing Process, Router ID: 193.5.80.113
 Supports only single TOS (TOS0) routes
 This implementation conforms to RFC2328
 RFC1583Compatibility flag is disabled
 OpaqueCapability flag is disabled
 Initial SPF scheduling delay 200 millisec(s)
 Minimum hold time between consecutive SPFs 1000 millisec(s)
 Maximum hold time between consecutive SPFs 10000 millisec(s)
 Hold time multiplier is currently 1
 SPF algorithm last executed 22m47s ago
 SPF timer is inactive
 Refresh timer 10 secs
 Number of external LSA 9. Checksum Sum 0x0006261f
 Number of opaque AS LSA 0. Checksum Sum 0x00000000
 Number of areas attached to this router: 1

 Area ID: 0.0.0.0 (Backbone)
   Number of interfaces in this area: Total: 1, Active: 1
   Number of fully adjacent neighbors in this area: 2
   Area has message digest authentication
   SPF algorithm executed 22 times
   Number of LSA 12
   Number of router LSA 11. Checksum Sum 0x0004d611
   Number of network LSA 1. Checksum Sum 0x00006511
   Number of summary LSA 0. Checksum Sum 0x00000000
   Number of ASBR summary LSA 0. Checksum Sum 0x00000000
   Number of NSSA LSA 0. Checksum Sum 0x00000000
   Number of opaque link LSA 0. Checksum Sum 0x00000000
   Number of opaque area LSA 0. Checksum Sum 0x00000000
```

### OSPF Packet Capture
We captured the packets using `tshark -i ens4 -w capture.pcap`.
Then display it using `tshark -r capture.pcap -Y ospf -V`)
The detailed packet capture can be found in `assets/ospf-capture_2020-03-19_1830.txt`.

## Dynamic routing RIPv2 and OSPFv2
Enable both ripd and ospfd using systemctl.
Then reboot the router.

Using `show ip route` in `vtysh` we can see that both OSPF and RIP
have configured routes for our router.

```
Codes: K - kernel route, C - connected, S - static, R - RIP,
O - OSPF, I - IS-IS, B - BGP, A - Babel,
> - selected route, * - FIB route

R   0.0.0.0/0 [120/2] via 193.5.80.1, ens4, 20:17:25
O   0.0.0.0/0 [110/10] via 193.5.80.1, ens4, 20:17:49
S>* 0.0.0.0/0 [1/0] via 193.5.80.1, ens4
C>* 127.0.0.0/8 is directly connected, lo
O   193.5.80.0/24 [110/10] is directly connected, ens4, 20:17:56
C>* 193.5.80.0/24 is directly connected, ens4
R>* 193.5.81.224/27 [120/2] via 193.5.80.108, ens4, 20:17:47
C>* 193.5.82.128/27 is directly connected, ens3
R   193.5.82.192/27 [120/2] via 193.5.80.115, ens4, 20:17:23
O>* 193.5.82.192/27 [110/20] via 193.5.80.115, ens4, 20:17:49
O>* 193.5.85.128/27 [110/20] via 193.5.80.137, ens4, 20:17:49
O>* 193.5.85.160/27 [110/20] via 193.5.80.138, ens4, 20:17:49
O>* 193.5.86.160/27 [110/20] via 193.5.80.146, ens4, 20:17:49
R   193.5.86.224/27 [120/2] via 193.5.80.148, ens4, 20:17:28
O>* 193.5.86.224/27 [110/20] via 193.5.80.148, ens4, 20:17:49
O>* 193.5.87.96/27 [110/20] via 193.5.80.152, ens4, 20:17:49
O>* 193.5.87.128/27 [110/20] via 193.5.80.153, ens4, 20:17:49
```


The numbers in the brackets are to be interpreted as [
        <admin distance>^[As defined by Cisco: `rip=120`, `ospf=110`, `direct-connect=0`]
        / <metric>^[The "priority", the route will go in the direction of the gateway with the lowest metric]
].

We can change the admin distances using `distance ospf (intra-area|inter-area|external) <distance>`
for OSPF and `distance rip <distance>` for RIP.

We configure a static route as a fallback for the network `193.5.82.192/27` (Group N115)
using this command: `ip route 193.5.82.192/27 193.5.80.115 130`.

To test this, we disable both RIP and OSPF using `systemctl disable` and
flush our route table: `ip route flush table main`. Then `reboot`.

After the reboot, we can check our routes using `sudo route -n`:
```
Kernel IP routing table
Destination     Gateway         Genmask         Flags Metric Ref    Use Iface
0.0.0.0         193.5.80.1      0.0.0.0         UG    0      0        0 ens4
193.5.80.0      0.0.0.0         255.255.255.0   U     0      0        0 ens4
193.5.82.128    0.0.0.0         255.255.255.224 U     0      0        0 ens3
193.5.82.192    193.5.80.115    255.255.255.224 UG    0      0        0 ens4
```

And ping the internal router interface of group N115 `ping 193.5.82.193`:

```
PING 193.5.82.193 (193.5.82.193) 56(84) bytes of data.
64 bytes from 193.5.82.193: icmp_seq=1 ttl=64 time=2.55 ms
64 bytes from 193.5.82.193: icmp_seq=2 ttl=64 time=1.38 ms
^C
--- 193.5.82.193 ping statistics ---
2 packets transmitted, 2 received, 0% packet loss, time 1001ms
rtt min/avg/max/mdev = 1.388/1.970/2.552/0.582 ms
```

After this, re-enable OSPF and RIP.

# 2020-03-25
## IPv6 Connectivity
We check the connectivity using `ping -6 www.switch.ch`.
Using `ip -6 addr` we find that the internal interface (ens3) has a single
link scope address, while the external interface has a link scope,
and 2 global addresses.

In trying to obtain Router Advertisements (RA) we wanted to install `ndisc6`.
Not knowing yum, we accidentally upgraded the system,
which filled the host HDD to the brim (100% usage).
This caused the router domain to be paused.
After consulting with Mr.Wenger,
we opted to delete the windows client domain for now^[this frees roughly 30G of space].

The RAs were eventually captured in `assets/icmpv6-capture_2020-03-25_2015.txt`.

The Link Local Address (LLA) is `fe80::1`.

# 2020-04-01
## IPv6 Static Routing
To set up static routing, we ssh into the router as root.

Within vtysh do the following:
```
configure terminal
ipv6 route ::/0 fe80::1 ens4 250
exit
exit
write mem
```

We confirm this using `show ipv6 route` in vtysh:
```
Codes: K - kernel route, C - connected, S - static, R - RIPng,
O - OSPFv6, I - IS-IS, B - BGP, A - Babel,
> - selected route, * - FIB route

S>* ::/0 [250/0] via fe80::1, ens4
C>* ::1/128 is directly connected, lo
C * fe80::/64 is directly connected, ens3
C>* fe80::/64 is directly connected, ens4
```

Using `ping6 -I ens4 fe80::1` works as expected as well.

If we configure a global scope address for ens4 as well,
we can reach destinations in the internet:

```
configure terminal
interface ens4
ipv6 address 2001:620:500:FF00:FF0D/64
exit
exit
write mem
```

Using `ping6 -I ens4 www.switch.ch` confirms this working.

## IPv6 Router Advertisement
We choose to use Quagga, not radvd.
The following steps are run within vtysh:

```
configure terminal
interface ens4
ipv6 nd suppress-ra
exit
interface ens3
ipv6 address 2001:620:500:FF00::1/64
ipv6 address fe80::1/10
no ipv6 nd supress-ra
ipv6 nd prefix 2001:620:500:FF0D::/64
exit
ipv6 forwarding
exit
write mem
```

Using `ip -6 addr` we can verify that we receive ipv6 addresses on the Client01.

# 2020-04-15
## IPv6 Router Advertisement
### Generate a Unique Local Unicast prefix
Using the procedure described on page 30 of the slides S20.05 we generate our prefix to be the following: `fd45:d297:90fe::/48`

To configure this prefix in vtysh:
```
confgure terminal
interface ens3
ipv6 nd prefix fd45:d297:90fe:1::/64
exit
exit
write mem
```

Our client learns this prefix.
```
user@client01:~$ ip -6 addr
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 state UNKNOWN qlen 1000
inet6 ::1/128 scope host 
valid_lft forever preferred_lft forever
2: ens3: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 state UP qlen 1000
inet6 fd45:d297:90fe:1:ac9c:194f:f232:8adf/64 scope global temporary dynamic 
valid_lft 604790sec preferred_lft 86006sec
inet6 fd45:d297:90fe:1:7a7b:b6e6:da80:56af/64 scope global dynamic mngtmpaddr noprefixroute 
valid_lft 2591990sec preferred_lft 604790sec
inet6 2001:620:500:ff0d:ac9c:194f:f232:8adf/64 scope global temporary dynamic 
valid_lft 604790sec preferred_lft 86006sec
inet6 2001:620:500:ff0d:ba8e:f5b3:d26a:2ddf/64 scope global dynamic mngtmpaddr noprefixroute 
valid_lft 2591990sec preferred_lft 604790sec
inet6 fe80::2c74:65ab:4387:6c36/64 scope link noprefixroute 
valid_lft forever preferred_lft forever
```

## IPv6 dynamic routing
Setup RIPng using

```
echo "log file /var/log/quagga/ripngd.log" > /etc/quagga/ripngd.conf
chown quagga:quagga /etc/quagga/ripngd.conf
systemctl enable --now ripngd
systemctl status ripngd
cat /var/log/quagga/ripngd.log
```

Configure RIPng in vtysh
```
conf terminal
router ripng
network ens4
route 2001:620:500:ff0d::/64
exit
exit
write mem
```

Using `ip -6 route` we can tell that our router learns new routes using RIPng

```
Codes: K - kernel route, C - connected, S - static, R - RIPng,
       O - OSPFv6, I - IS-IS, B - BGP, A - Babel,
       > - selected route, * - FIB route

R>* ::/0 [120/2] via fe80::1, ens4, 00:01:27
S   ::/0 [250/0] via fe80::1, ens4
C>* ::1/128 is directly connected, lo
C>* 2001:620:500:ff00::/64 is directly connected, ens4
R>* 2001:620:500:ff03::/64 [120/2] via fe80::5054:ff:fe02:8ca3, ens4, 00:01:27
R>* 2001:620:500:ff04::/64 [120/2] via fe80::5054:ff:fe9d:1391, ens4, 00:01:27
R>* 2001:620:500:ff06::/64 [120/2] via fe80::ff06, ens4, 00:01:27
R>* 2001:620:500:ff08::/64 [120/2] via fe80::ff08, ens4, 00:01:27
C>* 2001:620:500:ff0d::/64 is directly connected, ens3
R>* 2001:620:500:ff0e::/64 [120/2] via fe80::5054:ff:fe02:8cd2, ens4, 00:01:27
R>* 2001:620:500:ff0f::/64 [120/2] via fe80::5054:ff:fe02:8cd3, ens4, 00:01:27
R>* 2001:620:500:ff10::/64 [120/2] via fe80::5054:ff:fe02:8cd4, ens4, 00:01:27
R>* 2001:620:500:ff12::/64 [120/2] via fe80::5054:ff:fe02:8cd6, ens4, 00:01:27
R>* 2001:620:500:ff14::/64 [120/2] via fe80::5054:ff:fe02:8cd8, ens4, 00:01:27
R>* 2001:620:500:ff80::/64 [120/2] via fe80::1, ens4, 00:01:27
R>* fd32:6534:c5e4::/64 [120/2] via fe80::5054:ff:fe02:8cd6, ens4, 00:01:27
R>* fd4b:e45c:56b7::/64 [120/2] via fe80::5054:ff:fe02:8ca3, ens4, 00:01:27
R>* fd62:5985:5de4::/64 [120/2] via fe80::5054:ff:fe02:8cd2, ens4, 00:01:27
R>* fde0:9297:84c::/48 [120/2] via fe80::ff08, ens4, 00:01:27
R>* fde2:32a6:6a01:ff00::/64 [120/2] via fe80::1, ens4, 00:01:27
R>* fde2:32a6:6a01:ff80::/64 [120/2] via fe80::1, ens4, 00:01:27
C>* fe80::/10 is directly connected, ens3
C * fe80::/64 is directly connected, ens4
C>* fe80::/64 is directly connected, ens3
```

We can test the connectivity using `ping -6 www.switch.ch`

```
user@client01:~$ ping -6 www.switch.ch
PING www.switch.ch(prod.www.switch.ch (2001:620:0:ff::5c)) 56 data bytes
64 bytes from prod.www.switch.ch (2001:620:0:ff::5c): icmp_seq=1 ttl=54 time=4.19 ms
64 bytes from prod.www.switch.ch (2001:620:0:ff::5c): icmp_seq=2 ttl=54 time=4.04 ms
64 bytes from prod.www.switch.ch (2001:620:0:ff::5c): icmp_seq=3 ttl=54 time=4.97 ms
64 bytes from prod.www.switch.ch (2001:620:0:ff::5c): icmp_seq=4 ttl=54 time=4.92 ms
```

# 2020-04-22
## DHCP server
First we configure server01, by setting `/etc/sysconfig/network` to:
```
NETWORKING=yes
NETWORKING_IPV6=yes
NOZEROCONF=yes
GATEWAY=193.5.82.129
IPV6_DEFAULTDEV=ens3
IPV6_DEFAULTGW=FE80::1
```

and `/etc/sysconfig/network-scripts/ifcfg-ens3` to:
```
BOOTPROTO=static
DEVICE=ens3
ONBOOT=yes
PREFIX=27
IPADDR=193.5.82.130
IPV6INIT=yes
IPV6_AUTOCONF=no
IPV6ADDR=2001:620:500:FFOD::20/64
NM_CONTROLLED=no
```

Set the hostname: `hostnamectl set-hostname ns.n113.nslab.ch`.

Set `/etc/dhcp/dhcpd.conf` to:
```
subnet 193.5.82.128 netmask 255.255.255.224 {
         option routers 193.5.82.129;

         pool {
           option domain-name-servers 193.5.82.130,193.5.80.80;
           max-lease-time 300;
           range 193.5.82.144 193.5.82.158;
           allow unknown-clients;
         }
}
```
then start the DHCP service using `systemctl start dhcp`
and verify startup using `tail -50 /var/log/messages`.

We can use client01 to verify our configuration (make sure the values are not statically configured).
We can then use `systemd-resolve --status` to see that it uses our two desired name servers:
```
DNS Servers: 193.5.82.130
             193.5.80.80
```

and `ip -4 addr` to see that we get assigned an address in the DHCP pool.

Now that everything works, we can enable DHCPD on the DHCP server:
`systemctl enable dhcpd`

To add a reservation we append the following to `/etc/dhcp/dhcpd.conf`:

```
host client01 {
  hardware ethernet 52:54:00:35:84:52
  fixed-address 193.5.82.150
}
```

to apply this, restart the DHCPD service: `systemctl restart dhcpd`

We can check if this works on client01 as follows:
```
user@client01:~$ sudo dhclient -r; sudo dhclient
user@client01:~$ ip -4 addr
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
2: ens3: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    inet 193.5.82.150/27 brd 193.5.82.159 scope global ens3
       valid_lft forever preferred_lft forever
```

The server stores the leases in `/var/lib/dhcpd/dhcpd.leases`.

# 2020-05-27
## DNS Server - Basic Configuration
Access server01 using `virsh console server01`.
Make sure the required packages are installed `rpm -qa | grep -e bind -e bind-libs -e bind-utils`.

Create a configuration with only loopback and root name server caching:
We used the given config in /etc/named.conf and add the following:
```
zone "localhost" IN {
    type hint;
    file "named.loopback";
};
```

We also changed listen-on, listen-on-v6, and allow-query from `localhost` to `any`.

Restart it using `systemctl restart named`

We can verify that our DNS server works by making sure that `dig @193.5.82.130 bfh.ch` and `dig bfh.ch` return the same result.

# 2020-06-03
## DNS Server - Zones
Add the following to /etc/named.conf
``` 
zone "n113.nslab.ch" IN {
  type hint;
  file "named.nslab";
};
```

Create a new zone file `/var/named/named.nslab` with the permissions 640 root:named.
```
$TTL 300
@                   IN          SOA     ns.n113.nslab.ch. hostmaster.nslab.ch. (
                    2020060300          ; Serial
                    300                 ; Refresh
                    300                 ; Retry
                    300                 ; Expire
                    300 )               ; Negative Caching
                    IN          NS      ns.n113.nslab.ch.

$ORIGIN n113.nslab.ch.
ns                  IN          A       193.5.82.130
                    IN          AAAA    2001:620:500:FF0D::20
```
# 2020-06-25
## DNS Server - Zones
Using nsupdate, add the PTR records for our server and router:
```
nsupdate -y DHCP_UPDATER:Qq6gGm8yExOc7ltYRutSV47prHBMiG2Ty9okFt1zEvLmwfBGZ8UEO3VyG5uq
> server ns.demonet.ch
> update add 130.82.5.193.in-addr.arpa. 300 IN PTR ns.n113.nslab.ch.
> show
> send
> quit
```

We first create a new zone in /etc/named.conf:
```
zone "d.0.f.f.0.0.5.0.0.2.6.0.1.0.0.2.ip6.arpa" {
  type master;
  file "named.reverse.nslab";
};
```

To create the IPv6 reverse zone, we create the file /var/named/named.reverse.nslab:
```
$TTL 300
@                   IN          SOA ns.n113.nslab.ch. hostmaster.nslab.ch. (
                    2020062500          ; Serial
                    300                 ; Refresh
                    300                 ; Retry
                    300                 ; Expire
                    300 )               ; Negative Caching
                    IN          NS ns.n113.nslab.ch.

$ORIGIN d.0.f.f.0.0.5.0.0.2.6.0.1.0.0.2.ip6.arpa.

0.2.0.0.0.0.0.0.0.0.0.0.0.0.0.0 PTR ns.n113.nslab.ch.
```

## DNS Server - adjust the resolver
We add the following to our DHCP configuration:
```
option domain-name-servers 193.5.82.130,193.5.80.80
```

## DNS/DHCP - Dynamic Updates
To configure the DNS to allow updates from DHCP, add the following to /etc/named.conf:
```
key DHCP_UPDATER {
  algorithm hmac-md5;
  secret Qq6gGm8yExOc7ltYRutSV47prHBMiG2Ty9okFt1zEvLmwfBGZ8UEO3VyG5uq;
};
```

In order to issue dynamic updates, add the following to the pool config in /etc/dhcp/dhcpd.conf

```
update-optimization false;
update-static-leases true;
key DHCP_UPDATER {
  algorithm hmac-md5;
  secret Qq6gGm8yExOc7ltYRutSV47prHBMiG2Ty9okFt1zEvLmwfBGZ8UEO3VyG5uq;
};
```

Using nslookup we can see that this works:
```
user@client01:~$ nslookup 193.5.82.150
150.82.5.193.in-addr.arpa       name = client01.
150.82.5.193.in-addr.arpa       name = client01.local.

Authoritative answers can be found from:

user@client01:~$ nslookup client01.local
Server:         127.0.0.53
Address:        127.0.0.53#53

Non-authoritative answer:
Name:   client01.local
Address: 193.5.82.150
Name:   client01.local
Address: 2001:620:500:ff0d:ba8e:f5b3:d26a:2ddf
...
```

## Preparing the mail server
Start the server02 vm using `virsh start server02`.
Connect to the server02 via ssh (find IP using dhcp leases on server01).
Enable virsh console from within server02:
```
sudo systemctl enable serial-getty@ttyS0.service
sudo systemctl start serial-getty@ttyS0.service
```

First we configure server02, by setting `/etc/sysconfig/network` to:
```
NETWORKING=yes
NETWORKING_IPV6=yes
NOZEROCONF=yes
GATEWAY=193.5.82.129
IPV6_DEFAULTDEV=ens3
IPV6_DEFAULTGW=FE80::1
```

and `/etc/sysconfig/network-scripts/ifcfg-ens3` to:
```
BOOTPROTO=static
DEVICE=ens3
ONBOOT=yes
PREFIX=27
IPADDR=193.5.82.131
IPV6INIT=yes
IPV6_AUTOCONF=no
IPV6ADDR=2001:620:500:FFOD::25/64
NM_CONTROLLED=no
```
Reboot and check the connectivity using ping and ping -6.

Complete the DNS zone entries (on server01:/var/named/named.nslab):
```
mail  IN    A     193.5.82.131
      IN    AAAA  2001:620:500:FFOD::25
```

## MTA - Receiving Mails
Change the following in /etc/postfix/main.cf:
```
myhostname = mail.n113.nslab.ch
mydomain = n113.nslab.ch
myorigin = $myhostname
mynetworks = 193.5.82.128/27, 127.0.0.0/8
inet_interfaces = all
mydestination = $mydomain, $myhostname, localhost.$mydomain, localhost
relay_destinations = $mydomain
```

To test this:
```
echo "Subject: Hello" | sendmail user@localhost
cat /var/spool/mail/user
From root@mail.n113.nslab.ch Thu Jun 25 17:00:26 2020
Return-Path: <root@mail.n113.nslab.ch>
X-Original-To: user@localhost
Delivered-To: user@localhost.n113.nslab.ch
Received: by mail.n113.nslab.ch (Postfix, from userid 0)
id 860B232A9DF8; Thu, 25 Jun 2020 17:00:26 +0200 (CEST)
Subject: Hello
Message-Id: <20200625150026.860B232A9DF8@mail.n113.nslab.ch>
Date: Thu, 25 Jun 2020 17:00:26 +0200 (CEST)
From: root@mail.n113.nslab.ch (root) 
```

Now we configure the MX record for the mail server:
```
@ 300 IN  MX  10  mail.n113.nslab.ch.
```

## MTA - Sending mails
We used the telnet method described in the previous sections documentation.
This works from the client01, but not from our personal computers.

## MTA - Access to mailboxes via IMAP3 (and POP3)
The package was already installed.

We create the file /etc/dovecot/local.conf with the following content:
```
# Location of mail folders (mbox) and mailbox (INBOX)
mail_location = mbox:~/.dcmail/:INBOX=/var/spool/mail/%u

# Additional group for privileged operations
mail_privileged_group = mail
# Additional group for INBOX access
mail_access_groups = mail

# in order to test via telnet
disable_plaintext_auth = no
ssl = no
```

Use systemd to start and enable dovecot.

Following [this tutorial](https://www.experts-exchange.com/articles/868/Testing-POP3-with-telnet.html) we tested POP3 via telnet.

## MTA - Configuration of a MUA
We were able to install thunderbird, log in with user and access and send emails.

## Install and configure a web server with LE certificates
To install apache: `yum install httpd php mod_ssl`

Create a file /etc/httpd/conf.d/mail.conf:
```
<VirtualHost *:80>
  ServerName mail.n113.nslab.ch
</VirtualHost>
```

Then start httpd using systemd.
Test the access using a web browser.

Install cerbot, using `yum install certbot python2-certbot-apache --enablerepo=epel`
and run it to obtain a certificate: `certbot --apache -d mail.n113.nslab.ch`.

Fill out the prompts as asked.

Check the access using a web broser.

## Securing the communication
Enable submission by putting the following into /etc/postfix/master.cf:
```
submission inet n       -       n       -       -       smtpd
  -o syslog_name=postfix/submission
  -o smtpd_tls_security_level=encrypt
  -o smtpd_tls_cert_file=/etc/letsencrypt/live/mail.n113.nslab.ch/fullchain.pem
  -o smtpd_tls_key_file=/etc/letsencrypt/live/mail.n113.nslab.ch/privkey.pem
```

Use systemd to start and enable saslauthd, and to restart postfix.

We can verify that it worked using thunderbird.
Just set up the account again, thunderbird will complain if no encryption.

In order to configure postfix to use TLS between mailservers, add the following to /etc/postfix/master.cf:
```
smtp      inet  n       -       n       -       -       smtpd
  -o smtpd_tls_security_level=may
  -o smtpd_tls_cert_file=/etc/letsencrypt/live/mail.n113.nslab.ch/fullchain.pem
  -o smtpd_tls_key_file=/etc/letsencrypt/live/mail.n113.nslab.ch/privkey.pem
```

In order to configure dovecot to secure receiving email, we set /etc/dovecot/local.conf to the following:
```
service imap-login {
  inet_listener imap {
    port = 0
  }
  inet_listener imaps {
    port = 993
    ssl = yes
  }
}

service pop3-login {
  inet_listener pop3 {
    port = 0
  }
  inet_listener pop3s {
    port = 995
    ssl = yes
  }
}

#Enable SSL/TLS (disable vulnerable SSL/TLS versions)
ssl = required
ssl_protocols = !SSLv3 !TLSv1
ssl_cipher_list = ALL:HIGH:!SSLv2:!MEDIUM:!LOW:!EXP:!RC4:!MD5:!aNULL:@STRENGTH

# Cert and key to use
ssl_cert = </etc/letsencrypt/live/mail.n113.nslab.ch/fullchain.pem
ssl_key = </etc/letsencrypt/live/mail.n113.nslab.ch/privkey.pem
```

(Be sure to remove the lines:
```
disable_plaintext_auth = no
ssl = no
```
)!

Using [checktls.com](https://www.checktls.com/TestReceiver?LEVEL=DETAIL&amp;EMAIL=mail.n113.nslab.ch) we can make sure that everything works.

## Access mailboxes via webmail
Make sure roundcubemail is installed.
If the access if forbidden (it was in our case),
add the following line to /etc/httpd/conf.d/roundcubemail.conf in section /usr/share/roundcubemail/installer > mod_authz_core:
```
Require ip 178.xxx.yyy.203
```

Verify the config using `apachectl configtest` and restart apache using `apachectl restart`.

Confingure roundcube using the installer:
- Database setup
  * db_dsnw
  * SQLite
  * /var/lib/roundcubemail/rc.db
- IMAP Settings:
  * default_host = ssl://mail.n113.nslab.ch
  * default_port = 993
- SMTP Settings:
  * smtp_server = tls://mail.n113.nslab.ch
  * smtp_port = 587
- Use the current IMAP username and password for SMTP authentication = true

Test the config and save it to /etc/roundcubemail/config.inc.php

Add the following line to /etc/httpd/conf.d/roundcubemail.conf in section /usr/share/roundcubemail > mod_authz_core (our own IPs):
```
Require ip 178.xxx.yyy.203
Require ip 80.xxx.yyy.24
```
